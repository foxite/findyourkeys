﻿using Game;
using UnityEngine;

namespace RPGCharacterAnims{
	
	public class RPGCharacterInputController : MonoBehaviour{
		public RPGInput current;

		//Inputs.
		[HideInInspector] public bool inputJump;
		[HideInInspector] public bool inputLightHit;
		[HideInInspector] public bool inputDeath;
		[HideInInspector] public bool inputAttackL;
		[HideInInspector] public bool inputAttackR;
		[HideInInspector] public bool inputStrafe;
		[HideInInspector] public float inputTargetBlock = 0;
		[HideInInspector] public float inputAimVertical = 0;
		[HideInInspector] public float inputAimHorizontal = 0;
		[HideInInspector] public float inputHorizontal = 0;
		[HideInInspector] public float inputVertical = 0;
		[HideInInspector] public bool inputAiming;
		[HideInInspector] public bool inputRoll;

		private bool m_Paused;

		private void Start() {
			PauseManager.Instance.Pause += (o, e) => {
				m_Paused = true;
			};
			PauseManager.Instance.Unpause += (o, e) => {
				m_Paused = false;
			};

			inputJump = false; //Input.GetButtonDown("Jump");
			inputLightHit = false; //Input.GetButtonDown("LightHit");
			inputDeath = false; //Input.GetButtonDown("Death");
			inputAttackL = false; //Input.GetButtonDown("AttackL");
			inputAttackR = false; //Input.GetButtonDown("AttackR");
			inputStrafe = false; //Input.GetKey(KeyCode.LeftShift);
			inputTargetBlock = 0; //Input.GetAxisRaw("TargetBlock");
			inputAimVertical = 0; //Input.GetAxisRaw("AimVertical");
			inputAimHorizontal = 0; //Input.GetAxisRaw("AimHorizontal");
			inputAiming = false; //Input.GetButton("Aiming");
			inputRoll = false; //Input.GetButtonDown("L3");
		}

		/// <summary>
		/// Input abstraction for easier asset updates using outside control schemes.
		/// </summary>
		protected virtual void Inputs() {
			if (m_Paused) {
				inputHorizontal = 0;
				inputVertical = 0;
			} else {
				inputHorizontal = Input.GetAxisRaw("Horizontal");
				inputVertical = Input.GetAxisRaw("Vertical");
			}
		}

		void Update(){
			Inputs();
			current = new RPGInput(){
				moveInput = CameraRelativeInput(inputHorizontal, inputVertical),
				aimInput = new Vector2(inputAimHorizontal, inputAimVertical),
				jumpInput = inputJump
			};
		}

		/// <summary>
		/// Movement based off camera facing.
		/// </summary>
		protected virtual Vector3 CameraRelativeInput(float inputX, float inputZ){
			//Forward vector relative to the camera along the x-z plane   
			Vector3 forward = Camera.main.transform.TransformDirection(Vector3.forward);
			forward.y = 0;
			forward = forward.normalized;
			//Right vector relative to the camera always orthogonal to the forward vector.
			Vector3 right = new Vector3(forward.z, 0, -forward.x);
			Vector3 relativeVelocity = inputHorizontal * right + inputVertical * forward;
			//Reduce input for diagonal movement.
			if(relativeVelocity.magnitude > 1){
				relativeVelocity.Normalize();
			}
			return relativeVelocity;
		}

		public bool HasAnyInput(){
			if(current.moveInput == Vector3.zero && current.aimInput == Vector2.zero && inputJump == false){
				return false;
			}
			else{
				return true;
			}
		}

		public bool HasMoveInput(){
			if(current.moveInput == Vector3.zero){
				return false;
			}
			else{
				return true;
			}
		}

		public bool HasAimInput(){
			if((current.aimInput.x < -0.8f || current.aimInput.x > 0.8f) || (current.aimInput.y < -0.8f || current.aimInput.y > 0.8f) || inputAiming){
				return true;
			}
			else{
				return false;
			}
		}
	}

	public struct RPGInput{
		public Vector3 moveInput;
		public Vector2 aimInput;
		public bool jumpInput;
	}
}