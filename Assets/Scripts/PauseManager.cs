﻿using System;
using UnityEngine;

namespace Game {
	public class PauseManager : MonoBehaviour {
		[SerializeField]
		private GameObject m_PauseScreen;

		public bool Paused { get; private set; }
		public static PauseManager Instance { get; private set; }

		public event EventHandler Pause;
		public event EventHandler Unpause;

		public PauseManager() {
			Instance = this;
		}

		private void Start() {
			m_PauseScreen.SetActive(false);
		}

		private void Update() {
			if (Input.GetButtonDown("Cancel")) {
				SetPaused(!Paused);
			}
		}

		public void SetPaused(bool newState) {
			if (Paused != newState) {
				Paused = newState;
				if (Paused) {
					Pause?.Invoke(this, null);
					m_PauseScreen.SetActive(true);
				} else {
					Unpause?.Invoke(this, null);
					m_PauseScreen.SetActive(false);
				}
			}
		}
	}
}
