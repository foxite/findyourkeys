﻿using UnityEngine;

public class EditorOnlyObjects : MonoBehaviour {
	private void Awake() {
		foreach (GameObject obj in GameObject.FindGameObjectsWithTag("EditorOnly")) {
			obj.SetActive(false);
		}
	}
}
