﻿using Game.ScriptableObjects;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI {
	public class InventorySlot : MonoBehaviour {
		[SerializeField]
		private Image m_ItemImage;
		
		public void SetItem(Item item) {
			m_ItemImage.sprite = item.inventorySprite;
		}

		public void SetItem(Sprite sprite) {
			m_ItemImage.sprite = sprite;
		}

		public Sprite GetSprite() {
			return m_ItemImage.sprite;
		}
	}
}
