﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI {
	public class DialogOption : MonoBehaviour {
		[SerializeField]
		private DialogManager m_DialogManager;
		[SerializeField]
		private Button m_Button;
		[SerializeField]
		private TMP_Text m_Text;

		public void SetOption(DialogManager dm, string optionKey, string text) {
			m_DialogManager = dm;
			m_Text.text = text;
			m_Button.onClick.AddListener(() => {
				if (optionKey == "_end") {
					m_DialogManager.RemoveNPC();
					PauseManager.Instance.SetPaused(false);
				} else {
					m_DialogManager.SetDialogKey(optionKey);
				}
			});
		}
	}
}
