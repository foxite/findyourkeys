﻿using UnityEngine;
using Game.ScriptableObjects;
using Newtonsoft.Json.Linq;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

namespace Game.UI {
	public class DialogManager : MonoBehaviour {
		public static DialogManager Instance { get; private set; }
		
		[SerializeField]
		private Text m_DialogText;
		[SerializeField]
		private Transform m_DialogOptions;
		[SerializeField]
		private GameObject m_DialogOptionPrefab;
		[SerializeField]
		private TabMenuItem m_DialogTab;

		private NPC m_DialogNPC;
		private JObject m_CurrentDialog;
		private Action m_OnExitDialogKey;

		public TabMenuItem DialogTab { get; private set; }

		public DialogManager() {
			Instance = this;
		}

		private void Awake() {
			PlayerPrefs.DeleteAll();
			DialogTab = m_DialogTab;
		}

		private bool StartConditionMatches(int index, int savedValue, string comparison, int testValue) {
			switch (comparison) {
				case "eq": return savedValue == testValue;
				case "ne": return savedValue != testValue;
				case "gt": return savedValue > testValue;
				case "lt": return savedValue < testValue;
				case "gte": return savedValue >= testValue;
				case "lte": return savedValue <= testValue;
				default:
					Debug.LogError($"Invalid comparison in start block item {index} of NPC {m_DialogNPC.npcName}", m_DialogNPC);
					return false;
			}
		}

		public void SetNPC(NPC npc) {
			m_DialogNPC = npc;

			JToken startBlock = m_DialogNPC.DialogJson["start"];

			string selectedKey = null;
			int index = 0;
			foreach (JToken option in startBlock) {
				JToken variableToken = option["variable"];
				if (variableToken != null) {
					// If a condition exists, test it, and select this option if it is true
					string variableName = variableToken.Value<string>();
					if (variableName != null) {
						int savedValue = PlayerPrefs.GetInt(variableName); // The actual value of the specified variable
						string comparison = option["comparison"].Value<string>(); // The comparison to use
						int testValue = option["value"].Value<int>(); // The value to compare it against

						if (StartConditionMatches(index, savedValue, comparison, testValue)) {
							// If it matches, take this key
							selectedKey = option["dialog"].Value<string>();
							break;
						}
					}
				} else {
					// If no condition exists, always take this option
					selectedKey = option["dialog"].Value<string>();
					break;
				}
				index++;
			}

			if (selectedKey == null) {
				Debug.LogError($"No start option matched the conditions of NPC {m_DialogNPC.npcName}", m_DialogNPC);
			} else {
				m_DialogTab.Unhide();
				SetDialogKey(selectedKey);
			}
		}

		public void RemoveNPC() {
			m_DialogNPC = null;
			m_DialogTab.Hide();
		}

		public void SetDialogKey(string key) {
			m_OnExitDialogKey?.Invoke();

			try {
				m_CurrentDialog = m_DialogNPC.DialogJson["dialog"][key].ToObject<JObject>();
			} catch (NullReferenceException) {
				// I have no idea why this happens, but this fixes it, and nothing else seems to happen.
				// Without this, pressing the exit option the second time you open a dialog will fail to close it, and log this NRE.
				// With this, it will close normally.
				// Why?
				// ¯\_(ツ)_/¯
				return;
			}
			m_DialogText.text = m_CurrentDialog["_dialog"].ToObject<string>();
			
			List<DialogOptionText> dots = new List<DialogOptionText>();

			foreach (KeyValuePair<string, JToken> token in m_CurrentDialog) {
				if (token.Key.StartsWith("_") && token.Key != "_end") {
					// Special
					if (token.Key.StartsWith("_set")) {
						// Set variable
						string variableName = token.Value["variable"].Value<string>();
						int variableValue = token.Value["value"].Value<int>();

						Action operation = () => {
							PlayerPrefs.SetInt(variableName, variableValue);
						};

						if (token.Key == "_setEnter") {
							// Set variable now
							operation();
						} else if (token.Key == "_setExit") {
							// Set variable when this is exited
							m_OnExitDialogKey = operation;
						}
					}
				} else {
					dots.Add(new DialogOptionText(token.Key, token.Value.ToObject<string>()));
				}
			}

			Util.UpdateHierarchyArray(m_DialogOptions, dots.Count, m_DialogOptionPrefab, (tf, j) => {
				tf.GetComponent<DialogOption>().SetOption(this, dots[j].option, dots[j].text);
			});
		}

		private class DialogOptionText {
			public string option;
			public string text;

			public DialogOptionText(string option, string text) {
				this.option = option;
				this.text = text;
			}
		}
	}
}
