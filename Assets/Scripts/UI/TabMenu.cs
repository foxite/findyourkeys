﻿using UnityEngine;

namespace Game.UI {
	public class TabMenu : MonoBehaviour {
		[SerializeField]
		private TabMenuItem m_DefaultActiveTab;

		private TabMenuItem m_ActiveTab;
		public TabMenuItem ActiveTab {
			get {
				return m_ActiveTab;
			}
			set {
				if (m_ActiveTab != null) {
					m_ActiveTab.Content.gameObject.SetActive(false);
				}
				m_ActiveTab = value;
				if (value != null) {
					// TODO replace sprite with active tab background
					value.Content.gameObject.SetActive(true);
				}
			}
		}

		private void Awake() {
			m_ActiveTab = m_DefaultActiveTab;
		}
	}
}
