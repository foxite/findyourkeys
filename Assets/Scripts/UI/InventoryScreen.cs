﻿using Game.Items;
using UnityEngine;

namespace Game.UI {
	/// <summary>
	/// Displays an InventoryManager on the screen and allows full interaction with it. Relies on a Layout Manager.
	/// </summary>
	public class InventoryScreen : MonoBehaviour {
		[SerializeField]
		private GameObject m_InventorySlotPrefab;
		[SerializeField]
		private InventoryManager m_Inventory;

		private void OnEnable() {
			if (m_Inventory != null) {
				UpdateInventory(m_Inventory);
			}
		}

		public void UpdateInventory(InventoryManager inventory) {
			Util.UpdateHierarchyArray(transform, inventory.Count, m_InventorySlotPrefab, (transform_, i) => {
				transform_.GetComponent<InventorySlot>().SetItem(inventory.GetItem(i));
			});
		}
	}
}
