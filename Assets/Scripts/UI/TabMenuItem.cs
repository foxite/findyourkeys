﻿using UnityEngine;

namespace Game.UI {
	/// <summary>
	/// This MonoBehaviour should be placed on the label of the tab.
	/// </summary>
	public class TabMenuItem : MonoBehaviour {
		[SerializeField]
		private RectTransform m_Content;
		[SerializeField]
		private TabMenu m_Menu;

		public RectTransform Content => m_Content;
		
		/// <summary>
		/// Hides the tab label and unsets the tab content as the active tab.
		/// </summary>
		public void Hide() {
			gameObject.SetActive(false);
			if (m_Menu.ActiveTab == this) {
				m_Menu.ActiveTab = null;
			}
		}

		/// <summary>
		/// Unhides the tab label.
		/// </summary>
		public void Unhide() {
			gameObject.SetActive(true);
		}

		/// <summary>
		/// Sets the tab as the active tab.
		/// </summary>
		public void Show() {
			m_Menu.ActiveTab = this;
		}
	}
}
