﻿using System;
using UnityEngine;

namespace Game.Characters {
	[RequireComponent(typeof(Animator))]
	public class PlayerController : MonoBehaviour {
		[SerializeField]
		private float m_MoveSpeed;

		// Animation
		private PlayerState m_State;

		// State
		protected bool Paused => PauseManager.Instance.Paused;

		// Components
		private Animator m_AniCmp;
		private CharacterController m_CC;

		private void Start() {
			m_State = PlayerState.Idle;
			m_AniCmp = GetComponent<Animator>();
			m_CC = GetComponent<CharacterController>();

			/*PauseManager.Instance.Pause += (o, e) => {
				//OnPause();
			};
			PauseManager.Instance.Unpause += (o, e) => {
				//OnUnpause();
			};*/
		}

		private void FixedUpdate() {
			if (Paused)
				return;

			// Get input
			float xAxis = Input.GetAxis("Horizontal");
			float zAxis = Input.GetAxis("Vertical");
			//bool btnInteract = Input.GetButton("Interact");
			//bool btnTalk = Input.GetButton("Talk");
			//bool btnUse = Input.GetButton("Use");

			Vector3 motionVector = new Vector3(xAxis, 0, zAxis);
			this.m_CC.Move(motionVector.normalized * m_MoveSpeed * Time.deltaTime);

			if (motionVector.magnitude != 0) {
				transform.forward = motionVector.normalized;
			}

			if (xAxis == 0 && zAxis == 0) {
				m_State = PlayerState.Idle;
			} else {
				m_State = PlayerState.Walking;
			}
			m_AniCmp.SetInteger("State", (int) m_State);
		}
	}

	public enum PlayerState {
		Idle, Walking // Might add more later.
	}
}
