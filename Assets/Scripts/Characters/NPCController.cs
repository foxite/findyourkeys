﻿using Game.ScriptableObjects;
using Game.UI;
using UnityEngine;

namespace Game.Characters {
	public class NPCController : MonoBehaviour {
		[SerializeField]
		private NPC m_TheNPC;

		private bool m_PlayerInRange = false;

		private void OnTriggerEnter(Collider other) {
			if (other.CompareTag("Player")) {
				m_PlayerInRange = true;
			}
		}

		private void Update() {
			if (m_PlayerInRange && Input.GetButtonDown("Talk")) {
				DialogManager.Instance.SetNPC(m_TheNPC);
				DialogManager.Instance.DialogTab.Show();
				PauseManager.Instance.SetPaused(true);
			}
		}

		private void OnTriggerExit(Collider other) {
			if (other.CompareTag("Player")) {
				m_PlayerInRange = false;
				DialogManager.Instance.RemoveNPC();
			}
		}
	}
}
