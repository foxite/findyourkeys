﻿using Cinemachine;
using UnityEngine;

namespace Game.Camera {
	public class RunescapeCamera : MonoBehaviour {
		[SerializeField] private CinemachineFreeLook m_Camera;
		[SerializeField] private bool m_InvertX, m_InvertY;
		[SerializeField] private float m_XSensitivity, m_YSensitivity;

		private void Update() {
			if (Input.GetMouseButton(2)) {
				var xaxis = m_Camera.m_XAxis;
				xaxis.Value = Input.GetAxis("Mouse X") * (m_InvertX ? -1 : 1) * m_XSensitivity;
				m_Camera.m_XAxis = xaxis;

				var yaxis = m_Camera.m_YAxis;
				yaxis.Value += Input.GetAxis("Mouse Y") * (m_InvertY ? -1 : 1) * m_YSensitivity;
				m_Camera.m_YAxis = yaxis;
			}
		}
	}
}
