﻿using System;
using System.Collections.Generic;
using Game.ScriptableObjects;
using UnityEngine;

namespace Game.Items {
	public class InventoryManager : MonoBehaviour {
		[SerializeField]
		private List<Item> m_Items;

		public int Count {
			get {
				return m_Items.Count;
			}
		}

		public int? MaximumSize { get; set; }
		public int? RoomLeft { get { return MaximumSize - m_Items.Count; } }

		public event EventHandler<ItemMovedData>   ItemMoved;
		public event EventHandler<ItemChangedData> ItemAdded;
		public event EventHandler<ItemChangedData> ItemRemoved;
		public event EventHandler<ItemChangedData> ItemChanged;
		public event EventHandler<ItemSwappedData> ItemSwapped;

		private void Awake() {
			if (m_Items == null) {
				m_Items = new List<Item>();
			}
		}

		public bool AddItem(Item item) {
			if (RoomLeft.HasValue && RoomLeft.Value != 0) {
				m_Items.Add(item);

				if (ItemAdded != null) {
					ItemAdded(this, new ItemChangedData(item, m_Items.Count - 1));
				}
				return true;
			}
			return false;
		}
		
		/// <returns>-1 if the item did not exist, otherwise the index of the removed item</returns>
		public int RemoveItem(Item item) {
			int index = m_Items.IndexOf(item);
			if (index != -1) {
				m_Items.RemoveAt(index);

				if (ItemRemoved != null) {
					ItemRemoved(this, new ItemChangedData(item, index));
				}
			}
			return index;
		}
		
		/// <returns>The item that was removed</returns>
		public Item RemoveItem(int index) {
			Item removed = m_Items[index];
			m_Items.RemoveAt(index);

			if (ItemRemoved != null) {
				ItemRemoved(this, new ItemChangedData(removed, index));
			}
			return removed;
		}

		/// <summary>
		/// Moves an item to a position by inserting it.
		/// </summary>
		public void MoveItem(int from, int to) {
			Item item = m_Items[from];
			m_Items.RemoveAt(from);
			m_Items.Insert(to, item);

			if (ItemMoved != null) {
				ItemMoved(this, new ItemMovedData(item, from, to));
			}
		}

		public void SwapItems(int a, int b) {
			Item item = m_Items[a];
			m_Items[a] = m_Items[b];
			m_Items.Insert(b, item);

			if (ItemSwapped != null) {
				ItemSwapped(this, new ItemSwappedData(m_Items[a], m_Items[b], a, b));
			}
		}
		
		/// <returns>If there was room and the item was added</returns>
		public bool InsertItem(int at, Item item) {
			if (RoomLeft.HasValue && RoomLeft.Value != 0) {
				m_Items.Insert(at, item);

				if (ItemAdded != null) {
					ItemAdded(this, new ItemChangedData(item, at));
				}
				return true;
			}
			return false;
		}

		public Item ReplaceItem(int at, Item item) {
			Item original = m_Items[at];
			m_Items[at] = item;

			if (ItemChanged != null) {
				ItemChanged(this, new ItemChangedData(item, at));
			}
			
			return original;
		}

		public Item GetItem(int index) {
			return m_Items[index];
		}

		public IEnumerator<Item> GetEnumerator() {
			return m_Items.GetEnumerator();
		}
	}

	public class ItemChangedData : EventArgs {
		public Item item;
		public int position;

		public ItemChangedData(Item item, int position) {
			this.item = item;
			this.position = position;
		}
	}
	
	public class ItemMovedData : EventArgs {
		public Item item;
		public int oldPosition;
		public int newPosition;

		public ItemMovedData(Item item, int oldPosition, int newPosition) {
			this.item = item;
			this.oldPosition = oldPosition;
			this.newPosition = newPosition;
		}
	}

	public class ItemSwappedData : EventArgs {
		public Item itemA;
		public Item itemB;
		public int newPositionA;
		public int newPositionB;

		public ItemSwappedData(Item itemA, Item itemB, int newPositionA, int newPositionB) {
			this.itemA = itemA;
			this.itemB = itemB;
			this.newPositionA = newPositionA;
			this.newPositionB = newPositionB;
		}
	}
}
