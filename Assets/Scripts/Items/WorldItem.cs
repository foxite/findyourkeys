﻿using Game.ScriptableObjects;
using UnityEngine;

namespace Game.Items {
	[RequireComponent(typeof(Collider))]
	public class WorldItem : MonoBehaviour {
		[SerializeField]
		private Item m_Item;

		private void Start() {
			if (m_Item == null) {
				Debug.LogWarning("WorldItem " + name + " does not have an assigned item");
			}

			if (!GetComponent<Collider>().isTrigger) {
				Debug.LogWarning("WorldItem " + name + " has a collider, but it is not a trigger.");
			}
		}

		private void OnTriggerEnter(Collider other) {
			InventoryManager otherIM = other.GetComponent<InventoryManager>();
			if (otherIM != null) {
				// Collided with a player
				otherIM.AddItem(m_Item);
			}
		}

		public void ApplyItemMeshMaterial() {
			if (m_Item != null) {
				GetComponent<MeshFilter>().mesh = m_Item.worldMesh;
				GetComponent<MeshRenderer>().material = m_Item.worldMaterial;
			} else {
				Debug.LogWarning("WorldItem " + name + " does not have an item assigned. Mesh/material will be set to None.");
				GetComponent<MeshFilter>().mesh = null;
				GetComponent<MeshRenderer>().material = null;
			}
		}
	}
}
