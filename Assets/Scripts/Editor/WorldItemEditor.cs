﻿using Game.Items;
using UnityEditor;
using UnityEngine;

namespace Game.EditorScripts {
	[CustomEditor(typeof(WorldItem)), CanEditMultipleObjects]
	public class WorldItemEditor : Editor {
		public override void OnInspectorGUI() {
			base.OnInspectorGUI();
			if (EditorGUILayout.DropdownButton(new GUIContent("Apply mesh/material"), FocusType.Keyboard)) {
				((WorldItem) target).ApplyItemMeshMaterial();
			}
		}
	}
}