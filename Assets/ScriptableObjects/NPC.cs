﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Game.ScriptableObjects {
	[CreateAssetMenu(fileName = "New NPC", menuName = "NPC")]
	public class NPC : ScriptableObject {
		public string npcName;
		public TextAsset dialogFile;

		public bool trades;
		public List<Item> tradeInventory;

		public JObject DialogJson { get; private set; }

		private void OnEnable() {
			if (dialogFile != null) {
				DialogJson = JObject.Parse(dialogFile.text);
			}
		}
	}
}
