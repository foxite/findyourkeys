﻿using UnityEngine;

namespace Game.ScriptableObjects {
	// This class contains everything relating to an actual item.
	// Other classes exist, such as WorldItem and InventoryItem, that contain a reference to this class. They are wrappers for this class.
	[CreateAssetMenu(fileName = "New Item", menuName = "Item")]
	public class Item : ScriptableObject  {
		public string itemName;
		public string description;
		/// <summary>
		/// Value if sold to a shop.
		/// If negative, this item cannot be sold.
		/// </summary>
		public int value;
		/// <summary>
		/// Value if bought from a shop.
		/// </summary>
		public int buyValue;
		public string category;

		public Sprite inventorySprite;

		public Mesh worldMesh;
		public Material worldMaterial;
	}
}
